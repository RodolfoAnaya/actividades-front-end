import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, pipe } from 'rxjs';

import { Pais } from '../interfaces/pais.interface';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private api: string='https://restcountries.com/v2';
  get getParams(){
    return new HttpParams()
    .set('fields','?fields=name,capital,numericCode,flag,population,region,alpha3Code');
  }
     
  constructor( private http: HttpClient) { }

  buscarPorPais(termino: string): Observable<Pais[]>{
    const url=`${this.api}/name/${termino}?fields=name,capital,numericCode,flag,population,region,alpha3Code`;
    return this.http.get<Pais[]>(url,{params:this.getParams});
  }
  buscarPorCapital(termino: string): Observable<Pais[]>{
    const url=`${this.api}/capital/${termino}?fields=name,capital,numericCode,flag,population,region,alpha3Code`;
    return this.http.get<Pais[]>(url,{params:this.getParams});
  }
  buscarPorRegion(termino: string): Observable<Pais[]>{

    const url=`${this.api}/region/${termino}?fields=name,capital,numericCode,flag,population,region,alpha3Code`;
    return this.http.get<Pais[]>(url,{params:this.getParams});
  }
  getPais(codigo: string): Observable<Pais>{
    const url=`${this.api}/alpha/${codigo}`;
    return this.http.get<Pais>(url);
  }
}
