import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pais } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styles: [
  ]
})
export class VerPaisComponent implements OnInit {

  pais!:Pais;
  codigo:string='';

  constructor(private aRoute: ActivatedRoute, private pService:PaisService) { }

  ngOnInit(): void {
    this.aRoute.params.subscribe(({codigo})=>{
      console.log(this.codigo)
      this.pService.getPais(codigo).subscribe(pais=>{this.pais=pais});
    });

  }

}
