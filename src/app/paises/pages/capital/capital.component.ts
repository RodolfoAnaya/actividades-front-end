import { Component, OnInit } from '@angular/core';
import { Pais } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-capital',
  templateUrl: './capital.component.html',
  styles: [
  ]
})
export class CapitalComponent implements OnInit {
  termino:string='';
  hayError:boolean=false;
  paises:Pais[]=[]
  constructor(private cService:PaisService) { }

  buscar(termino: string){
    this.hayError=false;
    this.termino=termino;
    this.cService.buscarPorCapital(this.termino).subscribe(resp=>{
    this.paises=resp;},
    (hE)=>{
      this.hayError=true;
      this.paises=[];
    })
  }

  ngOnInit(): void {
  }

}
