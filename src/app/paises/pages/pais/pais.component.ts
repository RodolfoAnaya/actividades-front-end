import { Component, OnInit } from '@angular/core';
import { Pais } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-pais',
  templateUrl: './pais.component.html',
  styles: [ 
    `
    a { cursor: pointer;}
    li {cursor:pointer;}
    `
  ]
})
export class PaisComponent implements OnInit {

  termino: string='';
  paises: Pais[]=[];
  paisesSugeridos: Pais[]=[];
  hayError: boolean=false;
  mostrarSugerencias:boolean=false;

  constructor(private pService:PaisService) { }

  buscar(termino: string){
    this.hayError=false;
    this.termino=termino;
    this.mostrarSugerencias=false;
    this.pService.buscarPorPais(this.termino).subscribe(resp=>{
    this.paises=resp;},
    (hE)=>{
      this.hayError=true;
      this.paises=[];
    })
  }

  sugerencias(termino:string){
    this.hayError=false;
    this.termino=termino;
    this.mostrarSugerencias=true;
    
    this.pService.buscarPorPais(termino)
    .subscribe(paises=>this.paisesSugeridos=paises.splice(0,5),
    (err)=>this.paisesSugeridos=[]
    );
  }

  buscarSugerido(termino:string){
    this.buscar(termino);
  }

  ngOnInit(): void {
  }
}
