import { Component, OnInit } from '@angular/core';
import { Pais } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styles: [`
    button {
      margin-right: 5px;
    }
    `
  ]
})

export class RegionComponent implements OnInit {

  hayError:boolean=false;
  termino:string='';
  paises:Pais[]=[];
  regionActiva:string="";
  regiones:string[]=[
    "Africa", "Americas", "Asia", "Europe", "Oceania",
  ];

  constructor(private rService:PaisService) { }

  activarRegion(region:string){
    if(region===this.regionActiva){return;}
    this.regionActiva=region;
    this.paises=[];
    this.rService.buscarPorRegion(region)
    .subscribe(paises=>this.paises=paises);
  }

  getClassCSS(region:string):string{
    return (region===this.regionActiva)? 'btn btn-primary':'btn btn-outline-primary'
  }

/*   buscar(termino: string){
    this.hayError=false;
    this.termino=termino;
    this.rService.buscarPorRegion(this.termino).subscribe(resp=>{
    this.paises=resp;},
    (hE)=>{
      this.hayError=true;
      this.paises=[];
    })
  } */

  ngOnInit(): void {
  }

}
