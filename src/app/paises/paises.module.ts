import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router'

import { CapitalComponent } from './pages/capital/capital.component';
import { PaisComponent } from './pages/pais/pais.component';
import { RegionComponent } from './pages/region/region.component';
import { VerPaisComponent } from './pages/ver-pais/ver-pais.component';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { TablesComponent } from './components/table-pais/table-pais.component';
import { TableCapitalComponent } from './components/table-capital/table-capital.component';
import { TableRegionComponent } from './components/table-region/table-region.component';




@NgModule({
  declarations: [
    CapitalComponent,
    PaisComponent,
    RegionComponent,
    VerPaisComponent,
    SearchInputComponent,
    TablesComponent,
    TableCapitalComponent,
    TableRegionComponent
  ],
  exports: [
    CapitalComponent,
    PaisComponent,
    RegionComponent,
    VerPaisComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ]
})
export class PaisesModule { }
