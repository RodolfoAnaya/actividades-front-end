import { Component, Input, OnInit } from '@angular/core';
import { Pais } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-table-capital',
  templateUrl: './table-capital.component.html',
  styles: [
  ]
})
export class TableCapitalComponent implements OnInit {

  @Input() paises:Pais[]=[];

  constructor() { }

  ngOnInit(): void {
  }

}
