import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject, debounceTime } from 'rxjs';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements OnInit {

  termino: string='';
  @Output() onNewPais: EventEmitter<string>=new EventEmitter();
  @Output() onDebounce:EventEmitter<string>=new EventEmitter();

  @Input() placeholder:string='';

  debouncer:Subject<string>=new Subject();

  constructor() { }

  buscar(){
    this.onNewPais.emit(this.termino);
  }

  teclaPresionada(){
    this.debouncer.next(this.termino);
  }

  ngOnInit(): void {
    this.debouncer
        .pipe(debounceTime(300))
        .subscribe(valor=>{
          this.onDebounce.emit(valor);
        })
  }

}
