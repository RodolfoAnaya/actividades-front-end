import { Component, Input, OnInit } from '@angular/core';
import { Pais } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-table-pais',
  templateUrl: './table-pais.component.html',
  styleUrls: [
    
  ]
})
export class TablesComponent implements OnInit {

  @Input() paises: Pais[]=[];

  constructor() { }

  ngOnInit(): void {
  }

}
