import { Component, Input, OnInit } from '@angular/core';
import { Pais } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-table-region',
  templateUrl: './table-region.component.html',
  styleUrls: []
})
export class TableRegionComponent implements OnInit {

  @Input() paises:Pais[]=[];

  constructor() { }

  ngOnInit(): void {
  }

}
